
## Image Labeling

# https://pypi.org/project/labelImg/
labelImg I installed into conda environment.


## Training Data Augmentation?


## Model Training

Can train [slowly] locally using the Docker image defined in `docker/tfobj`.


### Model Training (GPU, AWS)

Also, trained on a g3s.xlarge instace.

Deep Learning AMI (Ubuntu 16.04) Version 25.0 (ami-0b5768dbb07139929)
Description
MXNet-1.5.0, TensorFlow-1.14, PyTorch-1.2, Keras-2.2, Chainer-6.1, configured with NVIDIA CUDA, cuDNN, NCCL, Intel MKL-DNN, Docker & NVIDIA-Docker.


## Rectifying a Detection

### Jupyter Notes with OpenCV, as easy from PowerShell
```
$here=$(pwd)
docker run `
    --interactive --tty --init --rm `
    --name opencv-notebook `
    --publish 8888:8888 --volume ${here}:/app/data `
    alexlouden/python-opencv-notebook
```

* https://arnab.org/blog/so-i-suck-24-automating-card-games-using-opencv-and-python
* maybe http://nessy.info/?p=1202


### TODO

* Threshold for high intensities?
* Find lines using Hough transform... (from class)
* (Optional) Cluster lines into 4 classes (edges) using K-means or K-medians
* Find (any) enclosed boxes.
