import numpy as np
import scipy as sp
import keras
import matplotlib.pyplot as plt

import os
import sys

from keras.preprocessing.image import ImageDataGenerator

from train_color_shape_hw4p2 import preprocess_input, postprocess_input, get_image_data_generator, INPUT_SHAPE
from train_color_shape_hw4p2 import get_model

# IMAGE_DIR = "../data/set-cards-ktreleav-fully-labeled/train/"
IMAGE_DIR = "../src/color-shape-ktreleav/train"

HAS_OTHER = True

# LABEL_ORDER is a constant now.  Re-transcribe per training.
# Better approach is to enforce desired order at training.
if HAS_OTHER:
    MODEL_LABEL_ORDER = [
        'green-diamond',
        'green-oval',
        'green-squiggle',
        'other',
        'purple-diamond',
        'purple-oval',
        'purple-squiggle',
        'red-diamond',
        'red-oval',
        'red-squiggle'
    ]
else:
    MODEL_LABEL_ORDER = [
        "green-diamond",
        "green-oval",
        "green-squiggle",
        "purple-diamond",
        "purple-oval",
        "purple-squiggle",
        "red-diamond",
        "red-oval",
        "red-squiggle"
    ]


LABEL_REVERSE_LOOKUP = {
    label: k for k, label in enumerate(MODEL_LABEL_ORDER)
}


def load_model(*, weights_file="SET_model_weights.h5"):
    model = get_model()
    # model.load_weights("model_weights_8200.h5")
    model.load_weights(weights_file)
    return model


def generate_labeled_images(image_dir):
    for label in os.listdir(image_dir):
        label_dir = os.path.join(image_dir, label)
        for fn in os.listdir(label_dir):
            image_path = os.path.join(label_dir, fn)
            yield load_image(image_path), label


def load_image(image_path):
    img = keras.preprocessing.image.load_img(
        image_path,
        grayscale=False,
        color_mode='rgb',
        target_size=INPUT_SHAPE,
        interpolation='nearest'
    )
    np_img = np.array(img)
    return np_img


def get_confusion():
    from sklearn.metrics import classification_report, confusion_matrix

    batch_size = 500
    validation_generator = get_image_data_generator("stanford-cars-data/validation", batch_size)

    images, labels = next(validation_generator)

    n = len(MODEL_LABEL_ORDER)
    confusion = np.zeros((n, n))

    # Confution Matrix and Classification Report
    y_label = np.argmax(labels, axis=1)
    y_pred_ = model.predict(images)
    y_pred = np.argmax(y_pred_, axis=1)

    M = confusion_matrix(y_label, y_pred)
    print('Confusion Matrix')
    print(M)


def predict_class_index(model, image_batch):
    return np.argmax(predict_posteriors(model, image_batch), axis=1)


def predict_posteriors(model, image_batch):
    return model.predict(image_batch)


if False:
    # Demo: See some of the images in a class
    class_index = 6
    image_indices = np.argwhere(y_train == class_index)[:10][:, 0]

    for i in image_indices:
        plt.imshow(train_images[i, :, :].transpose(), cmap="gray")
        plt.show()
        # y_train[n][0]


def take(it, n):
    iterator = iter(it)
    for i in range(n):
        yield next(iterator)


def to_cat(data_generator):
    for batch, labels in data_generator:
        yield batch, np.argmax(labels, axis=1)


def generate_image_data(image_dir=IMAGE_DIR, input_shape=INPUT_SHAPE):
    for np_image, label in generate_labeled_images(image_dir):
        Y, X = input_shape
        batch = np_image.reshape((1, Y, X, 3))
        labels = np.array([LABEL_REVERSE_LOOKUP[label]])
        yield batch, labels


# def main():
if __name__ == "__main__":
    model = load_model()
    model.summary()

    batch_size = 4

    datagen = get_image_data_generator(IMAGE_DIR, batch_size=batch_size)     # already pre-processed
    batch, labels = next(datagen)
    labels_idx = np.argmax(labels, axis=1)

    batch_post = postprocess_input(batch)
    # batch_post = batch

    labels_ = predict_class_index(model, batch)

    for k in range(batch_size):
        plt.figure()
        plt.imshow(batch_post[k, :, :, :])
        plt.title("got {}; actual: {}".format(MODEL_LABEL_ORDER[labels_[k]], MODEL_LABEL_ORDER[labels_idx[k]]))
        plt.show()



    # main()
