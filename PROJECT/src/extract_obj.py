import os
import xml.etree.ElementTree as ET

import cv2
from util import slicetool
from PIL import Image


class ObjectsImage(object):
    """A utility class for enumerating images labeled with labelImages.
    
    # TODO(ktreleav): Cache all these properties.
    
    """
    def __init__(self, image_file):
        self.image_file = image_file
        self._image = None
        self._etree = None

    @property
    def image_dir(self):
        return os.path.dirname(self.image_file)

    @property
    def image_name(self):
        basename = os.path.basename(self.image_file)
        return os.path.splitext(basename)[0]

    @property
    def image(self):
        if self._image is None:
            # Needed to prevent `imread` from re-orienting the image data.
            # After that, I needed to convince to load a color image instead of B&W.
            img = cv2.imread(self.image_file, cv2.IMREAD_IGNORE_ORIENTATION | cv2.IMREAD_COLOR)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            self._image = img
        return self._image

    @property
    def xml_file(self):
        xml_file = "{}.xml".format(self.image_name)
        return os.path.join(self.image_dir, xml_file)

    @property
    def etree(self):
        if self._etree is None:
            self._etree = ET.parse(self.xml_file)
        return self._etree

    @property
    def xml_root(self):
        return self.etree.getroot()

    @property
    def xml_objects(self):
        return list(self.xml_root.iter("object"))

    @property
    def objects(self):
        return [ImageObject(self, obj) for obj in self.xml_objects]


class ImageObject(object):

    def __init__(self, objects_image, xml_object):
        self.objects_image: ObjectsImage = objects_image
        self.xml_object = xml_object

    @property
    def image(self):
        return self.objects_image.image[self.window]

    @property
    def window(self):
        return self.get_slice(self.parse_bbox(self.xml_object.find("bndbox")))

    @property
    def label(self):
        return self.xml_object.find("name").text

    @classmethod
    def get_slice(cls, box):
        xmin, xmax = box["xmin"], box["xmax"]
        ymin, ymax = box["ymin"], box["ymax"]
        return slicetool[ymin:ymax, xmin:xmax]

    @classmethod
    def parse_bbox(cls, bndbox):
        return {
            attr: int(bndbox.find(attr).text)
            for attr in ["xmin", "xmax", "ymin", "ymax"]
        }


def generate_object_images(image_dir):
    """Generate ``ObjectImage`` instance from a directory of labeled images."""
    for fn in os.listdir(image_dir):
        filepath = os.path.join(image_dir, fn)
        try:
            Image.open(filepath)
            yield ObjectsImage(filepath)

        except IOError:
            pass
