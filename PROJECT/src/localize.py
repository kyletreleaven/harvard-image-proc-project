import cv2
import numpy as np

import geom

from cached_property import cached_property
from geom import find_boxes
from geom_cv2 import four_point_transform
from sklearn.cluster import KMeans, DBSCAN
from util_cv2 import resize


class LocalCardDetector(object):
    """Algorithm to detect a card "locally", i.e., in a zoomed in and cropped image."""
    
    def get_thresholds(self, image):
        # https://en.wikipedia.org/wiki/Otsu%27s_method
        hi, _ = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        lo = .5 * hi
        return lo, hi

    def detect_edges(self, image):
        """Detect edges in the image.
        
        Assumes image is a grayscale (single-channel) image.
        
        Returns:
            (
                image: possibly resized,
                canny: the canny image (with edges in high intensity),
                ratio: the ratio of old size to new size                
            )

        """
        return self.smoothing_detect_edges(image)

    def smoothing_detect_edges(self, image):
        """Detect edges by:

        1) sizing to specific height
        2) applying Gaussian blur
        3) applying Canny edge detection

        Returns:
            (
                resized image,
                canny image,
                resize ratio
            )

        """
        ratio = image.shape[0] / 500.0
        image = image.copy()
        image = resize(image, height=500)
        # blur the image, and find edges
        gray = image
        gray = cv2.GaussianBlur(gray, (9, 9), 0)
        return image, cv2.Canny(gray, 75, 200), ratio
    
    def simple_detect_edges(self, image):
        lo, hi = self.get_thresholds(image)
        # hi = 200  # actually, tune by hand
        return image, cv2.Canny(image, .5 * hi, hi), 1.0
    
    def get_lines(self, canny_image):
        """Detect lines in a Canny image."""
        # return self.get_lines_simple_thresold(canny_image)
        return self.bisect_get_lines(canny_image)
    
    def get_lines_simple_thresold(self, canny_image):
        # This Hough threshold yields a reasonable number of lines that contain the desired ones.
        # ...sometimes.        
        # Appropriate threshold seems to be a function of the image...
        # Do we need an intelligent way to choose it?
        hough_t = 10
        return cv2.HoughLines(canny_image, 1, np.pi / 180, hough_t)
    
    def bisect_get_lines(self, canny_image, *, n_target=10, plus_or_minus=1):
        """Detect lines using Hough transform

        Uses bisection search to find an appropriate number.

        """
        def lines(t):
            t_ = int(t)
            return cv2.HoughLines(canny_image, 1, np.pi / 180, t_)

        def _f(t):
            lines_ = lines(t)
            n = 0 if lines_ is None else lines_.shape[0]
            return float(n - n_target)

        import scipy.optimize
        hough_t = scipy.optimize.bisect(_f, 1, 500, xtol=float(plus_or_minus))
        return lines(hough_t)
    
    def _filter_lines(self, lines):
        """Use K-means clustering to find 4 edges of a box."""
        dbscan = DBSCAN(eps=1.0, min_samples=1)
        dbscan.fit(lines[:, 0, :])
        labels = set(dbscan.labels_)
        n = len(labels)
        print("found {} classes".format(n))
        return self._filter_lines_kmeans(lines, n)
        return kmeans.cluster_centers_.reshape((-1, 1, 2))

    def _filter_lines_kmeans(self, lines, n=4):
        """Use K-means clustering to find 4 edges of a box."""
        kmeans = KMeans(n)
        kmeans.fit(lines[:, 0, :])
        return kmeans.cluster_centers_.reshape((-1, 1, 2))

    def get_boxes_from_canny(self, canny_image):
        lines = self.get_lines(canny_image)
        lines_ = self._filter_lines(lines)
        return find_boxes(lines_)
    
    @staticmethod
    def warp(image, contour):
        return four_point_transform(image, contour)

    def get_boxes(self, image, gray_code=cv2.COLOR_RGB2GRAY):
        """Detect and rectify the card in a focused image."""
        img_gray = cv2.cvtColor(image, gray_code)
        _, canny, ratio = self.detect_edges(img_gray)
        for box in self.get_boxes_from_canny(canny):
            yield box.reshape(4, 2) * ratio

    def detect(self, image, gray_code=cv2.COLOR_RGB2GRAY):
        """Detect and rectify the card in a focused image."""
        img_gray = cv2.cvtColor(image, gray_code)
        _, canny, ratio = self.detect_edges(img_gray)
        # Get boxes.
        boxes = list(self.get_boxes(image, gray_code))
        # Choose the largest box after intersection with the bounding box.
        H, W, _ = image.shape
        bbox = [(0, 0), (H, 0), (H, W), (0, W)]
        areas = [geom.area(geom.poly_intersection(box, bbox)) for box in boxes]
        box = boxes[np.argmax(areas)]
        return self.warp(image, box)


class LocalizeCard(object):

    def __init__(self, image):
        self.image = image

    @property
    def diagonal_length(self):
        return np.linalg.norm(self.image.shape[:2])

    @cached_property
    def gray_image(self):
        return cv2.cvtColor(self.image, cv2.COLOR_RGB2GRAY)

    @cached_property
    def canny_reference_image(self):
        ref_image, _, _ = self._canny
        return ref_image

    @cached_property
    def edges(self):
        _, canny, _ = self._canny
        return canny

    @cached_property
    def canny_ratio(self):
        _, _, ratio = self._canny
        return ratio

    @cached_property
    def _canny(self):
        return LocalCardDetector().smoothing_detect_edges(self.gray_image)

    @cached_property
    def lines(self):
        ref_lines = LocalCardDetector().bisect_get_lines(self.edges, n_target=10, plus_or_minus=1)
        ref_lines = ref_lines[:, 0, :]
        lines = np.dot(ref_lines, np.diag([self.canny_ratio, 1.]))
        return lines

    @cached_property
    def filtered_lines(self):
        # Find the number of clusters.
        dbscan = DBSCAN(eps=.05, min_samples=1)
        dbscan.fit(self._lines_normalized)
        n = len(set(dbscan.labels_))

        # Get cluster centers.
        kmeans = KMeans(n)
        kmeans.fit(self.lines)
        return kmeans.cluster_centers_

    @property
    def _lines_normalized(self):
        T = np.diag([1. / self.diagonal_length, 1. / np.pi])
        return np.dot(self.lines, T)

    @staticmethod
    def get_box_lines(lines):
        n, _ = lines.shape
        return lines.reshape((n, 1, -1))
