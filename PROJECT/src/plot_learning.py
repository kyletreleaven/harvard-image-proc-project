import matplotlib.pyplot as plt


def show_results(history):
    class Results:
        pass

    results = Results()
    results.history = history

    # plot loss
    plt.figure(figsize=(5, 5))
    plt.plot(results.history["loss"], label='Train loss')
    plt.plot(results.history["val_loss"], label='Val loss')
    plt.plot(np.argmin(results.history["val_loss"]), np.min(results.history["val_loss"]), marker='x', color='r',
             label='Best model')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.title('Learning Curve')
    plt.show()

    # plot accuracy
    plt.figure(figsize=(5, 5))
    plt.plot(results.history["acc"], label='Train acc')
    plt.plot(results.history["val_acc"], label='Val acc')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.title('Accuracy Curve')
    plt.show()

    print("Final top 1 accuracy:", results.history["val_top1_acc"][-1])
    print("Final top 5 accuracy:", results.history["val_top5_acc"][-1])


if __name__ == "__main__":
    import numpy as np

    history = np.load("results/training/training_history_classify_w_other.npz")
    show_results(history)
