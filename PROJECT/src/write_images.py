from collections import namedtuple
import os

from typing import Iterable

from PIL import Image
import matplotlib.pyplot as plt


LabeledImage = namedtuple("LabeledImage", "name image label")


def write_images(
        target_dir,
        labeled_images: Iterable[LabeledImage],
        image_format="png"
) -> None:
    """Write images into categorical directory structure for model training.

    Not totally clear what the best approach is.

    # https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.imsave.html
    # https://stackoverflow.com/questions/42393261/python-how-to-write-a-single-channel-png-file-from-numpy-array

    """
    os.makedirs(target_dir, exist_ok=True)
    for example in labeled_images:
        label_dir = os.path.join(target_dir, example.label)
        os.makedirs(label_dir, exist_ok=True)
        image_file = os.path.join(label_dir, "{}.{}".format(example.name, image_format))
        if False:
            img = Image.fromarray(example.image)
            img.save(image_file)
        else:
            plt.imsave(image_file, example.image)
