from enum import Enum


class _slicetool(object):
    def __getitem__(self, sl):
        return sl


class _slicing(_slicetool, Enum):
    slicetool = 0


slicetool = _slicing.slicetool
