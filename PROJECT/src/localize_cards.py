import os
import numpy as np
from util_cv2 import load_image, save_image


def side_by_side(img1, img2):
    h1, w1, c = img1.shape
    h2, w2, c_ = img2.shape
    dtype = img1.dtype
    assert c_ == c
    assert img2.dtype == dtype
    out = np.zeros((max(h1, h2), w1 + w2, c), dtype=dtype)
    out[:h1, :w1, :] = img1
    out[:h2, -w2:, :] = img2
    return out


# def main():
if __name__ == "__main__":

    # Create and save model.
    # Load datasets.
    import sys
    image_dir, target_dir = sys.argv[1:3]
    os.makedirs(target_dir, exist_ok=True)

    from localize import LocalCardDetector
    ld = LocalCardDetector()

    for fn in os.listdir(image_dir):
        image_src = os.path.join(image_dir, fn)
        image_dst = os.path.join(target_dir, fn)
        img = load_image(image_src)
        # img_ = cv2.hconcat([img, ld.detect(img)])
        img_ = side_by_side(img, ld.detect(img))
        print(image_dst)
        save_image(image_dst, img_)


if __name__ == "__main__":

    def main(): pass

    main()
