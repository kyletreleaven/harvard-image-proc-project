from collections import namedtuple
import json
import os

import numpy as np

from keras import models
from keras import optimizers
from keras.layers import Conv2D, BatchNormalization as BatchNorm, MaxPool2D, Flatten
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import InceptionV3
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Dropout
from keras.layers.normalization import BatchNormalization
from keras import optimizers
from keras.utils import to_categorical
from keras.metrics import top_k_categorical_accuracy
import functools

Data = namedtuple("Data", "train_dir test_dir")

INPUT_SHAPE = (299, 299)
# N_TARGETS = 9   # +1 for bicycles...
N_TARGETS = 10

# INPUT_SHAPE = (28, 28)
# N_TARGETS = 26


# preprocessing for InceptionV3 architecture
def preprocess_input(x):
    x = (x-128)/128
    return x


# revert back to [0,255] range for display
def postprocess_input(x):
    x = (x*128)+128
    x = np.clip(x,0,255)
    x = x.astype("uint8")
    return x


def get_image_data_generator(image_dir, batch_size=None):

    datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode="nearest"
    )

    return datagen.flow_from_directory(
        image_dir,
        target_size=INPUT_SHAPE,
        batch_size=batch_size or 20,
        class_mode="categorical"
    )


def get_model(*, input_shape=INPUT_SHAPE, n_targets=N_TARGETS):
    Y, X = input_shape
    input_shape = (Y, X, 3)
    base_model = InceptionV3(input_shape=input_shape, weights="imagenet", include_top=False)

    # When adding layers on top of the base model.
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation="relu")(x)
    x = BatchNormalization()(x)

    """
    With the code from the homework prompt I got the following warning:

    WARNING:tensorflow:Large dropout rate: 0.7 (>0.5). 
    In TensorFlow 2.x, dropout() uses dropout rate instead of keep_prob. 
    Please ensure that this is intended.

    So I switched to dropout rate of 0.3.
    """
    x = Dropout(0.3)(x)

    predictions = Dense(n_targets, activation="softmax")(x)  # gives softmax scores

    # set up model
    return Model(inputs=base_model.input, outputs=predictions)


def prepare_model_for_fine_tuning(model):
    for k, layer in enumerate(model.layers, 1):
        layer.trainable = (k >= 63)


def compile_model(model):
    my_optimizers = [
        "rmsprop",
        optimizers.RMSprop(lr=1e-4),
        # https://arxiv.org/pdf/1805.09967.pdf
        optimizers.SGD(lr=0.001, momentum=0.6, nesterov=True)
    ]

    top_1_acc = functools.partial(top_k_categorical_accuracy, k=1)
    top_1_acc.__name__ = "top1_acc"
    top_5_acc = functools.partial(top_k_categorical_accuracy, k=5)
    top_5_acc.__name__ = "top5_acc"

    opts = dict(
        metrics=[
            "acc",
            top_1_acc,
            top_5_acc,
        ]
    )

    model.compile(optimizer=my_optimizers[2], loss='categorical_crossentropy', **opts)


def train(model, data: Data, n_epochs):
    batch_size = 32

    train_generator = get_image_data_generator(data.train_dir, batch_size)
    validation_generator = get_image_data_generator(data.test_dir, batch_size)

    n_train = len(train_generator.classes)

    steps_per_epoch = n_train / batch_size
    # validation_steps = 15
    validation_steps = 50

    return model.fit_generator(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=n_epochs,
        validation_data=validation_generator,
        validation_steps=validation_steps
    )


def create_label_sequence(datagen):
    """Given the data generator, compute the class order of labels."""
    result = [None] * len(datagen.class_indices)
    for label in datagen.class_indices:
        result[datagen.class_indices[label]] = label
    return result


def main():

    N_EPOCHS = 1

    # Create and save model.
    model = get_model()
    model_json = model.to_json()
    with open("model.json", "w") as f:
        json.dump(model_json, f)

    model.summary()

    # prepare_model_for_fine_tuning(model)

    if True:
        compile_model(model)

        # Load datasets.
        image_dir = "color-shape-ktreleav"
        data = Data(
            train_dir=os.path.join(image_dir, "train"),
            test_dir=os.path.join(image_dir, "test"),
        )

        history = train(model, data, n_epochs=N_EPOCHS)
        model.save("model_weights.h5")
        np.savez("training_history.npz", **history.history)


if __name__ == "__main__":
    main()
