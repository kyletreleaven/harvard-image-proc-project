import os
import shutil

from typing import Callable

import extract_obj
import write_images


def extract_and_write_objects(
        image_dir: str,
        target_dir: str,
        *,
        label_fn: Callable[[str], str] = None,
):
    """Extract and write labeled objects from images.

    First, extracts labeled objects from a directory of labeled images.
    (Assumes xml files in labelImg format.)

    Then, writes objects into the common categorical directory structure.

    Optionally, takes a function to transform the object's label.

    """
    object_images = extract_obj.generate_object_images(image_dir)

    label_fn = label_fn or (lambda x: x)

    def generate_labeled_images():
        for oi in object_images:
            for k, obj in enumerate(oi.objects):
                yield write_images.LabeledImage(
                    name="{}_{}".format(obj.objects_image.image_name, k),
                    image=obj.image,
                    label=label_fn(obj.label)
                )

    write_images.write_images(target_dir, generate_labeled_images())


def relabel_images(
        image_dir: str,
        target_dir: str,
        label_fn: Callable[[str], str]
) -> None:
    """Relabel some labeled images.

    https://stackoverflow.com/questions/1591579/how-to-update-modify-an-xml-file-in-python

    """
    os.makedirs(target_dir, exist_ok=True)
    for oi in extract_obj.generate_object_images(image_dir):
        # Relabel in memory xml.
        for obj in oi.objects:
            label_element = obj.xml_object.find("name")
            label_element.text = label_fn(label_element.text)

        # Write.
        image_file_ = os.path.join(target_dir, os.path.basename(oi.image_file))
        shutil.copy(oi.image_file, image_file_)

        xml_file_ = os.path.join(target_dir, os.path.basename(oi.xml_file))
        oi.etree.write(xml_file_)


def sort_xml_into_train_and_test(image_dir: str, target_dir: str):
    train_dir = os.path.join(target_dir, "train")
    test_dir = os.path.join(target_dir, "test")
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)

    object_images = list(extract_obj.generate_object_images(image_dir))
    import random
    random.shuffle(object_images)

    train_split = int(.8 * len(object_images))

    def _copy(oi, dst_dir):
        image_file = os.path.join(dst_dir, os.path.basename(oi.image_file))
        shutil.copy(oi.image_file, image_file)
        xml_file = os.path.join(dst_dir, os.path.basename(oi.xml_file))
        oi.etree.write(xml_file)

    for oi in object_images[:train_split]:
        _copy(oi, train_dir)

    for oi in object_images[train_split:]:
        _copy(oi, test_dir)
