import os
import cv2
import numpy as np
import tensorflow as tf
import sys

import matplotlib
matplotlib.use('TkAgg')

script_dir = os.path.dirname(__file__)
object_detection_path = os.path.abspath(os.path.join(script_dir, "../../research/object_detection"))
sys.path.append(object_detection_path)

# Import utilites
# from utils import label_map_util
from utils import visualization_utils as vis_util

import serve_classifier

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'inference_graph'

# Grab path to current working directory
CWD_PATH = os.getcwd()

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
CHECKPOINT_FILE = 'frozen_inference_graph.pb'
LABELMAP_FILE = 'labelmap.pbtxt'
# Number of classes the object detector can identify
NUM_CLASSES = 1


class DetectionModel(object):

    def __init__(self, *, checkpoint_file=CHECKPOINT_FILE):
        self.category_index = {
            0: {
                "id": 0,
                "name": "card",
            }
        }

        # Load the Tensorflow model into memory.
        self.detection_graph = detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(checkpoint_file, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        # Define input and output tensors (i.e. data) for the object detection classifier

        # Input tensor is the image
        self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        # Output tensors are the detection boxes, scores, and classes
        # Each box represents a part of the image where a particular object was detected
        self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

        # Each score represents level of confidence for each of the objects.
        # The score is shown on the result image, together with the class label.
        self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

        # Number of objects detected
        self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    def detect(self, image_batch):
        """

        # Perform the actual detection by running the model with the image as input

        Args:
            image_batch.shape == (batch_size, Y, X, 3)

        Returns:
            (boxes, scores, classes, num)

        """
        sess = tf.compat.v1.Session(graph=self.detection_graph)
        return sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_batch}
        )


class Classifier(object):

    def __init__(self, SET_model):
        self.model = SET_model
        self.ordered_labels = serve_classifier.MODEL_LABEL_ORDER
        self.category_index = {
            k: {
                "id": k,
                "name": label
            }
            for k, label in enumerate(self.ordered_labels)
        }
        self.target_image_size = serve_classifier.INPUT_SHAPE

    def classify(self, image, boxes, *, use_normalized_coordinates=True):
        """Classify the objects defined by the image cropped to bounding boxes.

        The image must be RGB! That is what the classifier was trained on.

        Returns:
            classes, scores

        """
        batch = self.get_image_batch(image, boxes, use_normalized_coordinates=use_normalized_coordinates)
        posts = self.get_posteriors(batch)
        # Better return as np.array but I can convert later
        return np.argmax(posts, axis=1), np.max(posts, axis=1)

    def get_posteriors(self, image_batch):
        return self.model.predict(image_batch)

    def get_image_batch(self, image, boxes, *, use_normalized_coordinates=True):
        """Construct an image batch.

        The image must be RGB! That is what the classifier was trained on.

        """
        h, w = self.target_image_size
        n = len(boxes)
        image_batch = np.zeros((n, h, w, 3))
        for k, box in enumerate(boxes):
            image_ = self.crop_to_box(image, box, use_normalized_coordinates)
            image_ = self.resize_image(image_)
            image_batch[k, :, :, :] = image_
        # Pre-process the image.
        return serve_classifier.preprocess_input(image_batch)

    @staticmethod
    def crop_to_box(image, box, use_normalized_coordinates=True):
        if use_normalized_coordinates:
            im_height, im_width = image.shape[:2]
            ymin, xmin, ymax, xmax = box
            left, right, top, bottom = (int(xmin * im_width), int(xmax * im_width),
                                        int(ymin * im_height), int(ymax * im_height))
        else:
            top, left, bottom, right = box

        # print(left, right, top, bottom)
        return image[top:bottom, left:right]

    def resize_image(self, image):
        return cv2.resize(image, self.target_image_size, interpolation=cv2.INTER_CUBIC)


if __name__ == "__main__":

    # Create a Detection model.
    detection_model = DetectionModel()

    # Boxes filter method.
    def filter_boxes(boxes, scores, min_score_thresh=0.7):
        boxes_out = []
        scores_out = []
        for box, score in zip(boxes, scores):
            if score < min_score_thresh:
                continue
            boxes_out.append(box)
            scores_out.append(score)

        return boxes_out, scores_out

    # Create a classifier.
    classifier_model = serve_classifier.load_model(weights_file="results/weights/model_weights_classify_w_other.h5")
    classifier = Classifier(classifier_model)

    def get_annotated_image(image, boxes, classes, scores):
        image_ann2 = image.copy()
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_ann2,
            np.array(boxes2),
            np.array(classes2).astype(np.int32),
            np.array(scores2),
            classifier.category_index,
            use_normalized_coordinates=True,
            line_thickness=2,
            min_score_thresh=min_score_thresh
        )
        return image_ann2


    # Load image using OpenCV and
    # IMAGE_DIR = "../data/set-cards-ktreleav-fully-labeled/train"
    # IMAGE_DIR = "../data/bicycledata_resize"
    IMAGE_DIR = "../data/all-cards-xml/test"

    # PATH_TO_IMAGE = "color-shape-ktreleav/test/green-oval/20191028_212044_1.png"
    PATH_TO_IMAGE = "../data/set-cards-ktreleav-fully-labeled/test/20191028_210720.jpg"
    PATH_TO_IMAGE2 = "../data/set-cards-ktreleav-fully-labeled/test/20191028_213156.jpg"

    OUTDIR = "quick-all"

    def my_resize(image):
        new_size = tuple(map(lambda x: int(.5 * x), image.shape[:2]))
        return cv2.resize(image, new_size, interpolation=cv2.INTER_CUBIC)


    if True:
        os.makedirs(OUTDIR, exist_ok=False)

        for jpg_file in os.listdir(IMAGE_DIR):
            if not jpg_file.endswith(".jpg"):
                continue

            image_path = os.path.join(IMAGE_DIR, jpg_file)
            image = cv2.imread(image_path)
            # image = my_resize(image)
            batch_ = np.expand_dims(image, axis=0)
            # Detect bounding boxes in the image.
            (boxes, scores, classes, num) = detection_model.detect(batch_)
            boxes, scores = filter_boxes(np.squeeze(boxes), np.squeeze(scores))
            if len(boxes) < 1:
                continue
            # Classify the objects in the bounding boxes.
            min_score_thresh = 0.70
            # Seems the detector was trained on BGR, whereas the classifier was trained on RGB!
            image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            # image_batch = classifier.get_image_batch(image_rgb, boxes)
            # posts = classifier.get_posteriors(image_batch)
            classes2, scores2 = classifier.classify(image_rgb, boxes)

            def resize_image_for_annotation(image):
                from util_cv2 import resize
                ratio = image.shape[0] / 500.0
                image = image.copy()
                image = resize(image, height=500)
                return image, ratio

            image2, _ = resize_image_for_annotation(image)
            boxes2 = boxes

            image_annotated = get_annotated_image(image2, boxes2, classes2, scores2)
            out_file = os.path.join(OUTDIR, jpg_file)
            cv2.imwrite(out_file, image_annotated)


    else:
        image = cv2.imread(PATH_TO_IMAGE)
        print("Original image shape: ", image.shape)
        image = my_resize(image)
        print("Resized to: ", image.shape[:2])

        if False:
            image2 = cv2.imread(PATH_TO_IMAGE2)
            # print(image.shape, image2.shape)
            # sys.exit(0)

            Y, X, c = image.shape
            batch = np.zeros((2, Y, X, c))
            batch[0, :, :, :] = image
            batch[1, :, :, :] = image2
            image_expanded = batch

        else:
            # expand image dimensions to have shape: [1, None, None, 3]
            # i.e. a single-column array, where each item in the column has the pixel RGB value
            image_expanded = np.expand_dims(image, axis=0)


        # Detect bounding boxes in the image.
        (boxes, scores, classes, num) = detection_model.detect(image_expanded)
        # num here, is number of boxes per image in the batch.

        if False:
            image_ann = get_annotated_image(image, boxes, classes, scores)
            # All the results have been drawn on image. Now display the image.
            cv2.imshow('Object detector', image_ann)
            # Press any key to close the image
            cv2.waitKey(0)
            # Clean up
            cv2.destroyAllWindows()

        boxes, scores = filter_boxes(np.squeeze(boxes), np.squeeze(scores))

        # Classify the objects in the bounding boxes.
        min_score_thresh = 0.70
        image_ann = image.copy()

        # Seems the detector was trained on BGR, whereas the classifier was trained on RGB!
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        image_batch = classifier.get_image_batch(image_rgb, boxes)
        posts = classifier.get_posteriors(image_batch)

        classes2, scores2 = classifier.classify(image_rgb, boxes)
        boxes2 = boxes

        if False:
            for box, label_idx in zip(boxes2, classes2):
                plt.figure()
                img = classifier.crop_to_box(image_rgb, box)
                plt.imshow(img)
                plt.title(classifier.category_index[label_idx]["name"])
                plt.show()

        if True:
            image_ann2 = get_annotated_image(image, boxes2, classes2, scores2)
            # All the results have been drawn on image. Now display the image.
            cv2.imshow('Object detector', image_ann2)

            cv2.waitKey(0)

            cv2.destroyAllWindows()
