from collections import namedtuple
import json
import os

import numpy as np

from keras import models
from keras import optimizers
# from keras.utils import to_categorical
from keras.layers import Conv2D, BatchNormalization as BatchNorm, MaxPool2D, Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator


Data = namedtuple("Data", "train_dir test_dir")


INPUT_SHAPE = (95, 95)


# preprocessing for InceptionV3 architecture
def preprocess_input(x):
    x = (x-128)/128
    return x


# revert back to [0,255] range for display
def postprocess_input(x):
    x = (x*128)+128
    x = np.clip(x,0,255)
    x = x.astype("uint8")
    return x


def get_image_data_generator(image_dir, batch_size=None):

    datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode="nearest"
    )

    return datagen.flow_from_directory(
        image_dir,
        target_size=INPUT_SHAPE,
        batch_size=batch_size or 20,
        class_mode="categorical"
    )


def get_model():
    """This is the model from HW4 for MNIST letters."""
    # input_shape = (28, 28, 1)
    Y, X = INPUT_SHAPE
    input_shape = (Y, X, 3)

    conv2d_opts = dict(
        activation="relu",
        padding="same",
        # padding="valid",
    )
    model = models.Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape, **conv2d_opts));
    model.add(BatchNorm())
    model.add(Conv2D(32, (3, 3), **conv2d_opts));
    model.add(BatchNorm())
    model.add(Conv2D(32, (5, 5), **conv2d_opts));
    model.add(BatchNorm())
    model.add(MaxPool2D((2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv2D(64, (3, 3), **conv2d_opts));
    model.add(BatchNorm())
    model.add(Conv2D(64, (3, 3), **conv2d_opts));
    model.add(BatchNorm())
    model.add(Conv2D(64, (5, 5), **conv2d_opts));
    model.add(BatchNorm())
    model.add(MaxPool2D((2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv2D(128, (5, 5), **conv2d_opts));
    model.add(BatchNorm())
    model.add(Flatten())
    model.add(Dropout(0.5))
    model.add(Dense(9, activation="softmax"))

    return model


def compile_model(model):
    my_optimizers = [
        "rmsprop",
        optimizers.RMSprop(lr=1e-4),
    ]

    opts = dict(
        metrics=["acc"]
    )

    model.compile(optimizer=my_optimizers[0], loss='categorical_crossentropy', **opts)


def train(model, data: Data):
    batch_size = 5

    train_generator = get_image_data_generator(data.train_dir, batch_size)
    validation_generator = get_image_data_generator(data.test_dir, batch_size)

    n_train = len(train_generator.classes)

    steps_per_epoch = n_train / batch_size
    validation_steps = 15

    n_epochs = 1

    return model.fit_generator(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=n_epochs,
        validation_data=validation_generator,
        validation_steps=validation_steps
    )


def main():

    # Create and save model.
    model = get_model()
    model_json = model.to_json()
    with open("model.json", "w") as f:
        json.dump(model_json, f)

    model.summary()

    compile_model(model)

    # Load datasets.
    image_dir = "color-shape"
    data = Data(
        train_dir=os.path.join(image_dir, "train"),
        test_dir=os.path.join(image_dir, "test"),
    )

    history = train(model, data)
    model.save("model_weights.h5")
    np.savez("training_history.npz", **history.history)


if __name__ == "__main__":
    main()
