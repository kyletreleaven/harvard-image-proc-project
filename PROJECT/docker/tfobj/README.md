
## Model Training (Local/Testing)

```powershell
# Build the image for local training and testing.
docker build -t tfobj .

# Run the image. (from tensorflow1/models directory)
$here=$(pwd)
docker run `
    --interactive --tty --rm `
    --name tf `
    --publish 8008:8888 --publish 6006:6006 `
    --volume ${here}:/app/data `
    tfobj bash
```

### tensorflow 1.13.1 gpu jupyter --- up to date with recent update...?

https://hub.docker.com/r/tensorflow/tensorflow/tags?page=1&name=1.13.1

```
docker pull tensorflow/tensorflow:1.13.1-py3-jupyter
docker run -p 8888:8888 tensorflow/tensorflow:1.13.1-py3-jupyter
docker run -i -t tensorflow/tensorflow:1.13.1-py3-jupyter
```

### Refs

* https://medium.com/@RouYunPan/how-to-use-tensorflow-gpu-with-docker-2b72f784fdf3
* https://towardsdatascience.com/tensorflow-object-detection-with-docker-from-scratch-5e015b639b0b
